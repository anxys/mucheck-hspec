hlint:
	(cd support; ~/.cabal/bin/hlint `find ../src -name \*.hs`)

clean-sandbox:
	- cabal sandbox hc-pkg unregister MuCheck-QuickCheck

sandbox:
	mkdir -p ../mucheck-sandbox
	cabal sandbox init --sandbox ../mucheck-sandbox

build:
	cabal sandbox init --sandbox ../mucheck-sandbox
	cabal build

install:
	cabal sandbox init --sandbox ../mucheck-sandbox
	cabal install

run:
	- rm *.tix
	cabal build sample-test
	./dist/build/sample-test/sample-test
	./dist/build/mucheck-hspec/mucheck-hspec -tix sample-test.tix Examples/HspecTest.hs

prepare:
	cabal haddock
	cabal check
	cabal sdist

clean:
	- rm Examples/*_*
	- rm *.log


module Examples.HspecTest where
import Test.Hspec
import Test.Hspec.Core.Runner

import qualified Test.Hspec.Core.Runner as Hspec
import Data.List(intercalate, isInfixOf)
qsort :: Ord a => [a] -> [a]
qsort [] = []
qsort (x:xs) = qsort l ++ [x] ++ qsort r
        where l = filter (<=x) xs
              r = filter (>x) xs

{-# ANN spec1 "Test" #-}
spec1 =  do
  describe "qsort1" $ do
      it "for (qsort [3,2])" $ do
        qsort [3,2] `shouldBe` [2,3]

{-# ANN spec2 "Test" #-}
spec2 =  do
  describe "qsort2" $ do
      it "for (qsort [3,2,1])" $ do
        qsort [3,2,1] `shouldBe` [1,2,3]

{-# ANN spec3 "Test" #-}
spec3 =  do
  describe "qsort3" $ do
      it "for (qsort [4,3,2,1])" $ do
        qsort [4,3,2,1] `shouldBe` [1,2,3,4]

{-
-- You can also do this, but then you wont be able to make use of the mutation
-- fail fast

{# ANN main "TestSupport" #}
main :: IO ()
main = spec

{# ANN spec "Test" #}
spec = hspecResult $ do
  describe "qsort" $ do
    context "qsort1" $ do
      it "for (qsort [3,2])" $ do
        qsort [3,2] `shouldBe` [2,3]
    context "qsort2" $ do
      it "for (qsort [3,2,1])" $ do
        qsort [3,2,1] `shouldBe` [1,2,3]
    context "qsort3" $ do
      it "for (qsort [4,3,2,1])" $ do
        qsort [4,3,2,1] `shouldBe` [1,2,3,4]
-}

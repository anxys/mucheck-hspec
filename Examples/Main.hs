module Main where
import Test.Hspec
import Examples.HspecTest
import Test.Hspec.Core.Runner

main :: IO ()
main = do hspecResult spec1
          hspecResult spec2
          hspecResult spec3
          return ()

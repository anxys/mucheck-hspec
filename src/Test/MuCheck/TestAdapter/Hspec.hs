{-# LANGUAGE StandaloneDeriving, DeriveDataTypeable, TypeSynonymInstances, MultiParamTypeClasses #-}
-- | Module for using Hspec tests
module Test.MuCheck.TestAdapter.Hspec where
import qualified Test.Hspec.Core.Runner as Hspec
import Test.MuCheck.TestAdapter

import Data.Typeable

deriving instance Typeable Hspec.Summary
type HspecSummary = Hspec.Summary

-- | Summarizable instance of `Hspec.Summary`
instance Summarizable HspecSummary where
  testSummary _mutant _test result = Summary $ _ioLog result
  isSuccess (Hspec.Summary { Hspec.summaryExamples = _, Hspec.summaryFailures = sf } ) = sf == 0

data HspecRun = HspecRun String

instance TRun HspecRun HspecSummary where
  genTest _m tstfn = "hspecResult " ++ tstfn
  getName (HspecRun str) = str
  toRun s = HspecRun s

  summarize_ _m = testSummary :: Mutant -> TestStr -> InterpreterOutput HspecSummary -> Summary
  success_ _m = isSuccess :: HspecSummary -> Bool
  failure_ _m = isFailure :: HspecSummary -> Bool
  other_ _m = isOther :: HspecSummary -> Bool

